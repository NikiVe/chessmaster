<?php

use Chessmaster\Bishop;
use Chessmaster\Deck\FEN;
use Chessmaster\Deck\TestFuncFor4;
use Chessmaster\King;
use Chessmaster\Knight;
use Chessmaster\TestFuncForUnit;
use Tester\Tester;

include __DIR__ . '/vendor/autoload.php';

echo '1.Bitboard - Король' . PHP_EOL;
Tester::start(__DIR__ . '/source/1.Bitboard - Король/', new TestFuncForUnit(King::class));

echo '2.Bitboard - Король' . PHP_EOL;
Tester::start(__DIR__ . '/source/2.Bitboard - Конь/', new TestFuncForUnit(Knight::class));

echo '3.Bitboard - Король' . PHP_EOL;
Tester::start(__DIR__ . '/source/3.Bitboard - FEN/', new FEN());

echo '1.Bitboard - Король' . PHP_EOL;
Tester::start(__DIR__ . '/source/4.Bitboard - Дальнобойщики/', new TestFuncFor4());