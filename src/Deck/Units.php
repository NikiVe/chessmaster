<?php


namespace Chessmaster\Deck;


use Chessmaster\Bishop;
use Chessmaster\Castle;
use Chessmaster\King;
use Chessmaster\Knight;
use Chessmaster\Queen;
use Chessmaster\ULong;
use Chessmaster\Unit;

class Units
{
    protected $type = '';

    public const AR_TYPES = [
        'R' => Castle::class,
        'B' => Bishop::class,
        'Q' => Queen::class,
         0  => Unit::class
    ];

    protected $arUnits = [];

    protected $isWhite;

    public function __construct($type) {
        $this->isWhite = $type === strtoupper($type);

        $this->type = self::AR_TYPES[$type] ?? self::AR_TYPES[0];
    }

    public function add(int $row, $position) {
        $this->arUnits[] = new $this->type($row * 8 + $position - 1, $this);
    }

    public function getPosition() {
        $result = ULong::zero();
        foreach ($this->arUnits as $unit) {
            $result = $result->or($unit->getPosition());
        }
        return $result;
    }

    public function getUnitsMove(array $arPositions): ULong {
        foreach ($this->arUnits as $unit) {
            if (get_class($unit) !== Unit::class) {
                return $unit->getMask($arPositions);
            }
        }
        return ULong::zero();
    }

    public function getSort(): int {
        return $this->type !== Unit::class ? $this->type::SORT : 0;
    }

    public function isWhite() : bool {
        return $this->isWhite;
    }

    public function getType() {
        return $this->type;
    }
}