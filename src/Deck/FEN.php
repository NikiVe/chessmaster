<?php

namespace Chessmaster\Deck;

class FEN implements \Tester\TestFunc
{
    public function run(string $values): string {
        $deck = self::buildDeck($values);

        return implode(PHP_EOL, $deck->getArUnitPositions());
    }

    public static function buildDeck(string $fen) {
        $deck = new Deck();

        $arPositions = array_reverse(explode('/', $fen));

        foreach ($arPositions as $num => $row) {
            $len = strlen($row);
            $curPos = 0;
            for ($i = 0; $i < $len; $i++) {
                if (is_numeric($row[$i])) {
                    $curPos += (int)$row[$i];
                } else {
                    $curPos++;
                    $deck->addUnit($row[$i], (int)$num, $curPos);
                }
            }
        }

        return $deck;
    }
}