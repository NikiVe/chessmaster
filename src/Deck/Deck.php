<?php


namespace Chessmaster\Deck;


use Chessmaster\ULong;
use Chessmaster\Unit;

class Deck
{
    protected $arUnits = [
        'P' => NULL,
        'N' => NULL,
        'B' => NULL,
        'R' => NULL,
        'Q' => NULL,
        'K' => NULL,
        'p' => NULL,
        'n' => NULL,
        'b' => NULL,
        'r' => NULL,
        'q' => NULL,
        'k' => NULL,
    ];

    public function addUnit(string $type, int $row, int $position) {
        if (!$this->arUnits[$type]) {
            $this->arUnits[$type] = new Units($type);
        }
        $this->arUnits[$type]->add($row, $position);
    }

    public function getArUnitPositions() {
        $result = [];
        foreach ($this->arUnits as $arUnit) {
            $result[] = is_object($arUnit) ? (string)$arUnit->getPosition() : '0';
        }
        return $result;
    }

    public function getUnitsMove() {
        $result = [];
        $bitmapPositions = $this->getBitmapPositions();
        foreach ($this->arUnits as $unit) {
            if ($unit && $unit->getType() !== Unit::class) {
                $result[$unit->getSort()] = $unit->getUnitsMove($bitmapPositions);
            }
        }
        krsort($result);
        return $result;
    }

    public function getBitmapPositions() {
        $bitmap = [ULong::zero(), ULong::zero()];
        foreach ($this->arUnits as $units) {
            if (is_object($units)) {
                $i = $units->isWhite() ? 0 : 1;
                $bitmap[$i] = $bitmap[$i]->or($units->getPosition($bitmap[$i]));
            }
        }
        return $bitmap;
    }
}