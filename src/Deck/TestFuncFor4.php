<?php

namespace Chessmaster\Deck;

use Tester\TestFunc;

class TestFuncFor4 implements TestFunc
{

    public function run(string $values): string {
        $deck = FEN::buildDeck($values);
        $arMove = $deck->getUnitsMove();
        $result = [];
        foreach ($arMove as $moves) {
            $result[] = (string)$moves;
        }
        return implode(PHP_EOL, $result);
    }
}