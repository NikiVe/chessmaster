<?php


namespace Chessmaster;


use Tester\TestFunc;

class TestFuncForUnit implements TestFunc
{
    protected string $unitName;

    public function __construct(string $unitName) {
        $this->unitName = $unitName;
    }

    public function run(string $values): string {
        /* @var Unit $unit */
        $unit = new $this->unitName($values);

        return $unit->getCount() . PHP_EOL . $unit->getMask();
    }
}