<?php

namespace Chessmaster;

use Chessmaster\ULong as I;

class Knight extends Unit
{
    public function getMask(array $arPositions = []) {
        $nA =  I::of("18374403900871474942");
        $nAB = I::of("18229723555195321596");
        $nH =  I::of("9187201950435737471");
        $nGH = I::of("4557430888798830399");

//          knightBits <<  6 | knightBits >> 10
//        | knightBits << 15 | knightBits >> 17
//        | knightBits << 17 | knightBits >> 15
//        | knightBits << 10 | knightBits >>  6

        return
            $nGH->and($this->position->shiftedLeft(6)->or($this->position->shiftedRight(10)))
                ->or(
                    $nH->and(
                        $this->position->shiftedLeft(15)->or($this->position->shiftedRight(17))))
                ->or(
                    $nA->and(
                        $this->position->shiftedLeft(17)->or($this->position->shiftedRight(15))))
                ->or(
                    $nAB->and(
                        $this->position->shiftedLeft(10)->or($this->position->shiftedRight(6))));
    }
}