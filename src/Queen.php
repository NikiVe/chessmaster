<?php


namespace Chessmaster;


class Queen extends UnitLong
{
    public const SORT = 1;

    public static function getArMoves() {
        return array_merge(Bishop::getArMoves(), Castle::getArMoves());
    }
}