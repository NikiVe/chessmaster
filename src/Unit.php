<?php


namespace Chessmaster;

use Chessmaster\Deck\Units;

class Unit
{
    public const SORT = 0;

    protected ULong $position;

    protected $parent;

    public function __construct(string $position, Units $parent = NULL) {
        $this->parent = $parent;

        $this->position = ULong::one()->shiftedLeft($position);
    }

    public function getCount(): int {
        $i = ULong::of($this->getMask());

        $count = 0;
        while($i->isPositive()) {
            if ($i->and(1)->isPositive()) {
                $count++;
            }
            $i = $i->shiftedRight(1);
        }
        return $count;
    }

    public function getMask(array $arPositions = []) {
        return '';
    }

    public static function getRow(ULong $p) {
        return ULong::of((int)floor(($p->getBitLength() - 1) / 8));
    }

    public function getPosition(): ULong {
        return $this->position;
    }
}