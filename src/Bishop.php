<?php


namespace Chessmaster;


class Bishop extends UnitLong
{
    public const SORT = 2;

    public static function getArMoves() {
        return [
            function(ULong $p) {return $p->shiftedLeft(9)->and('18374403900871474942');},
            function(ULong $p) {return $p->shiftedRight(9)->and('9187201950435737471');},
            function(ULong $p) {return $p->shiftedRight(7)->and('18374403900871474942');},
            function(ULong $p) {return $p->shiftedLeft(7)->and('9187201950435737471');},
        ];
    }
}