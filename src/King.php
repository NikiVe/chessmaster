<?php

namespace Chessmaster;

class King extends Unit
{
    public function getMask(array $arPositions = []) {
        $pL = $this->position->and("18374403900871474942");
        $pR = $this->position->and("9187201950435737471");

        //($pL << 7) | ($p << 8) | ($pR << 9) |
        //($pL >> 1) |             ($pR << 1) |
        //($pL >> 9) | ($p >> 8) | ($pR >> 7) ;

        return $pL->shiftedLeft(7)->or($this->position->shiftedLeft(8))->or($pR->shiftedLeft(9))
                ->or($pL->shiftedRight(1))->or($pR->shiftedLeft(1))
                ->or($pL->shiftedRight(9))->or($this->position->shiftedRight(8))->or($pR->shiftedRight(7));
    }
}