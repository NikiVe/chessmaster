<?php


namespace Chessmaster;


use Brick\Math\BigDecimal;
use Brick\Math\BigInteger;
use Brick\Math\BigNumber;
use Brick\Math\BigRational;
use Brick\Math\Exception\DivisionByZeroException;
use Brick\Math\Exception\MathException;
use Brick\Math\Internal\Calculator;
use Brick\Math\RoundingMode;

class ULong extends BigNumber
{
    /**
     * The value, as a string of digits with optional leading minus sign.
     *
     * No leading zeros must be present.
     * No leading minus sign must be present if the number is zero.
     *
     * @var string
     */
    private $value;

    /**
     * Creates a BigInteger of the given value.
     *
     * @param ULong|int|float|string $value
     *
     * @return ULong
     *
     * @throws MathException If the value cannot be converted to a BigInteger.
     *
     * @psalm-pure
     */
    public static function of($value) : ULong
    {
        return new self(parent::of($value)->__toString());
    }

    public function __construct($value) {
        $value = (string)$value;
        if (strlen(Calculator::get()->toBase($value, 2)) <= 64) {
            $this->value = $value;
        } else {
            $this->value = "0";
        }
    }

    /**
     * Checks if this number is equal to the given one.
     *
     * @param ULong|int|float|string $that
     *
     * @return bool
     */
    public function isEqualTo($that) : bool
    {
        return $this->compareTo($that) === 0;
    }

    /**
     * Returns the number of bits in the minimal two's-complement representation of this BigInteger, excluding a sign bit.
     *
     * For positive BigIntegers, this is equivalent to the number of bits in the ordinary binary representation.
     * Computes (ceil(log2(this < 0 ? -this : this+1))).
     *
     * @return int
     */
    public function getBitLength() : int
    {
        if ($this->value === '0') {
            return 0;
        }

        if ($this->isNegative()) {
            return $this->abs()->minus(1)->getBitLength();
        }

        return strlen($this->toBase(2));
    }

    /**
     * Returns a string representation of this number in the given base.
     *
     * The output will always be lowercase for bases greater than 10.
     *
     * @param int $base
     *
     * @return string
     *
     * @throws \InvalidArgumentException If the base is out of range.
     */
    public function toBase(int $base) : string
    {
        if ($base === 10) {
            return $this->value;
        }

        if ($base < 2 || $base > 36) {
            throw new \InvalidArgumentException(\sprintf('Base %d is out of range [2, 36]', $base));
        }

        return Calculator::get()->toBase($this->value, $base);
    }

    /**
     * Returns the absolute value of this number.
     *
     * @return BigInteger
     */
    public function abs() : BigInteger
    {
        return $this->isNegative() ? $this->negated() : $this;
    }

    /**
     * Returns the integer bitwise-and combined with another integer.
     *
     * This method returns a negative BigInteger if and only if both operands are negative.
     *
     * @param ULong|int|float|string $that The operand. Must be convertible to an integer number.
     *
     * @return ULong
     */
    public function and($that) : ULong
    {
        $that = ULong::of($that);

        return new ULong(Calculator::get()->and($this->value, $that->value));
    }

    /**
     * Returns the product of this number and the given one.
     *
     * @param ULong|int|float|string $that The multiplier. Must be convertible to a BigInteger.
     *
     * @return ULong The result.
     *
     * @throws MathException If the multiplier is not a valid number, or is not convertible to a BigInteger.
     */
    public function multipliedBy($that) : ULong
    {
        $that = ULong::of($that);

        if ($that->value === '1') {
            return $this;
        }

        if ($this->value === '1') {
            return $that;
        }

        $value = Calculator::get()->mul($this->value, $that->value);

        return new ULong($value);
    }

    /**
     * Returns the integer left shifted by a given number of bits.
     *
     * @param int $distance The distance to shift.
     *
     * @return ULong
     */
    public function shiftedLeft(int $distance) : ULong
    {
        if ($distance === 0) {
            return $this;
        }

        if ($distance < 0) {
            return $this->shiftedRight(- $distance);
        }

        return $this->multipliedBy(ULong::of(2)->power($distance));
    }

    /**
     * Returns the integer right shifted by a given number of bits.
     *
     * @param int $distance The distance to shift.
     *
     * @return ULong
     */
    public function shiftedRight(int $distance) : ULong
    {
        if ($distance === 0) {
            return $this;
        }

        if ($distance < 0) {
            return $this->shiftedLeft(- $distance);
        }

        $operand = ULong::of(2)->power($distance);

        if ($this->isPositiveOrZero()) {
            return $this->quotient($operand);
        }

        return $this->dividedBy($operand, RoundingMode::UP);
    }

    /**
     * Returns the result of the division of this number by the given one.
     *
     * @param ULong|int|float|string $that         The divisor. Must be convertible to a BigInteger.
     * @param int                        $roundingMode An optional rounding mode.
     *
     * @return ULong The result.
     *
     * @throws MathException If the divisor is not a valid number, is not convertible to a BigInteger, is zero,
     *                       or RoundingMode::UNNECESSARY is used and the remainder is not zero.
     */
    public function dividedBy($that, int $roundingMode = RoundingMode::UNNECESSARY) : ULong
    {
        $that = ULong::of($that);

        if ($that->value === '1') {
            return $this;
        }

        if ($that->value === '0') {
            throw DivisionByZeroException::divisionByZero();
        }

        $result = Calculator::get()->divRound($this->value, $that->value, $roundingMode);

        return new ULong($result);
    }


    /**
     * Returns the quotient of the division of this number by the given one.
     *
     * @param BigNumber|int|float|string $that The divisor. Must be convertible to a BigInteger.
     *
     * @return ULong
     *
     * @throws DivisionByZeroException If the divisor is zero.
     */
    public function quotient($that) : ULong
    {
        $that = ULong::of($that);

        if ($that->value === '1') {
            return $this;
        }

        if ($that->value === '0') {
            throw DivisionByZeroException::divisionByZero();
        }

        $quotient = Calculator::get()->divQ($this->value, $that->value);

        return new ULong($quotient);
    }

    /**
     * Returns the integer bitwise-or combined with another integer.
     *
     * This method returns a negative BigInteger if and only if either of the operands is negative.
     *
     * @param ULong|int|float|string $that The operand. Must be convertible to an integer number.
     *
     * @return ULong
     */
    public function or($that) : ULong
    {
        $that = ULong::of($that);

        return new ULong(Calculator::get()->or($this->value, $that->value));
    }

    /**
     * Returns this number exponentiated to the given value.
     *
     * @param int $exponent The exponent.
     *
     * @return ULong The result.
     *
     * @throws \InvalidArgumentException If the exponent is not in the range 0 to 1,000,000.
     */
    public function power(int $exponent) : ULong
    {
        if ($exponent === 0) {
            return ULong::one();
        }

        if ($exponent === 1) {
            return $this;
        }

        if ($exponent < 0 || $exponent > Calculator::MAX_POWER) {
            throw new \InvalidArgumentException(\sprintf(
                'The exponent %d is not in the range 0 to %d.',
                $exponent,
                Calculator::MAX_POWER
            ));
        }

        return new ULong(Calculator::get()->pow($this->value, $exponent));
    }

    /**
     * Returns a BigInteger representing one.
     *
     * @return ULong
     *
     * @psalm-pure
     */
    public static function one() : ULong
    {
        /** @psalm-suppress ImpureStaticVariable */
        static $one;

        if ($one === null) {
            $one = new ULong('1');
        }

        return $one;
    }

    public static function zero() : ULong
    {
        /** @psalm-suppress ImpureStaticVariable */
        static $zero;

        if ($zero === null) {
            $zero = new self('0');
        }

        return $zero;
    }

    /**
     * {@inheritdoc}
     */
    public function getSign() : int
    {
        return ($this->value === '0') ? 0 : (($this->value[0] === '-') ? -1 : 1);
    }

    /**
     * {@inheritdoc}
     */
    public function compareTo($that) : int
    {
        $that = self::of($that);

        if ($that instanceof self) {
            return Calculator::get()->cmp($this->value, $that->value);
        }

        return - $that->compareTo($this);
    }

    public function toBigInteger(): BigInteger
    {
        // TODO: Implement toBigInteger() method.
    }

    public function toBigDecimal(): BigDecimal
    {
        // TODO: Implement toBigDecimal() method.
    }

    public function toBigRational(): BigRational
    {
        // TODO: Implement toBigRational() method.
    }

    public function toScale(int $scale, int $roundingMode = RoundingMode::UNNECESSARY): BigDecimal
    {
        // TODO: Implement toScale() method.
    }

    public function toInt(): int
    {
        // TODO: Implement toInt() method.
    }

    public function toFloat(): float
    {
        // TODO: Implement toFloat() method.
    }

    /**
     * {@inheritdoc}
     */
    public function __toString() : string
    {
        return $this->value;
    }

    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }
}