<?php


namespace Chessmaster;


class Castle extends UnitLong
{
    public const SORT = 3;

    public static function getArMoves() {
        return [
            function(ULong $p) {return $p->shiftedLeft(8);},
            function(ULong $p) {return $p->shiftedRight(8);},
            function(ULong $p) {return self::moveRight($p);},
            function(ULong $p) {return self::moveLeft($p);},
        ];
    }

    public static function moveRight(ULong $p) {
        $row = self::getRow($p);
        $np = $p->shiftedRight(1);
        return self::getRow($np)->isEqualTo($row) ? $np : ULong::zero();
    }

    public static function moveLeft(ULong $p) {
        $row = self::getRow($p);
        $np = $p->shiftedLeft(1);
        return self::getRow($np)->isEqualTo($row) ? $np : ULong::zero();
    }
}