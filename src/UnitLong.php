<?php


namespace Chessmaster;

abstract class UnitLong extends Unit
{
    public static function getArMoves() {
        return [];
    }

    public function getMask(array $arPositions = []) {
        $arMoves = static::getArMoves();
        $mask = ULong::zero();

        foreach ($arMoves as $moveFunc) {
            $next = $moveFunc($this->position);
            $isAttack = false;
            while ($next->isPositive()) {

                if (!$next->and($arPositions[$this->parent->isWhite() ? 0 : 1])->isZero()) {
                    break;
                }

                if (!$next->and($arPositions[!$this->parent->isWhite() ? 0 : 1])->isZero()) {
                    $isAttack = true;
                }

                $mask = $mask->or($next);

                if ($isAttack) {
                    break;
                }

                /* @var ULong $next */
                $next = $moveFunc($next);
            }
        }
        return $mask;
    }

}